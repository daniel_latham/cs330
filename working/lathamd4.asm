;#######################
;Daniel Latham
;BID: l3li3l
;March 10, 2015
;CS330
;Dr. Hyatt
;
;
;Homework 4
;
;
;
;Directions:
;Must be using linux32(preferred) or linux64
;Run
;$: ./makerun32 lathamd4
;or
;$: ./makerun64 lathamd4
;or
;$: ./asm32 lathamd4
;$: ./lathamd4
%include "along32.inc"

section .data

EnterMsg    : db "Enter a number to add to the list (max list size = 100); enter 0 to see list in ascending order" ,0ah,0
ExitMsg     : db "The sorted list, in ascending order: "                                                           ,0ah,0
InputError1 : db "Input Error; Integers Only"                                                                      ,0ah,0
InputError2 : db "Input Error; Maximum list is 100"                                                                ,0ah,0
InputError3 : db "Input Error; Enter AT LEAST ONE Integer"                                                         ,0ah,0

arrayInt times 100 dd 0                      ;creates a new array with 100 values set to 0

section .text

global main

    main:

mov         ebx, 0
mov         ecx, 0                           ;sets counter to 0
mov         edx, EnterMsg                    ;moves EnterMsg to edx to be called by WriteString
call        WriteString                      ;prints EnterMsg
jmp         readInput                        ;calls readInput label

    readInput:                               ;reads the input

call        ReadInt                          ;reads integer
jo          invalidInput                     ;jumps if not an integer value
cmp         ecx, 100                         ;compares ecx and 100
jg          tooManyInts                      ;if ecx is already 100, then cannot add int to array, so jump to error
cmp         eax,0                            ;tests for the 0 input
je          L1                               ;jumps to L1 if zero
mov         [arrayInt + (ecx * 4)], eax      ;adds read integer to array
inc         ecx                              ;increments counter
jmp         readInput                        ;loops if input is not 0

    L1:

cmp         ecx, 2                           ;compares ecx to 2
jl          badInput                         ;jumps to badinput if less than 2
push        ecx                              ;pushes ecx for number of numbers to print
dec         ecx                              ;decrements ecx because 0-based indexing


    L2:
    
push        ecx                              ;saves outer loop count
mov         ebx, 0                           ;sets ebx to 0 because 0 based indexing

    L3:

mov         eax, [arrayInt + (ebx * 4)]      ;moves current value of array, as determined by value of 
                                             ;ebx*4, to eax
cmp         [arrayInt + (ebx * 4) + 4], eax  ;compares next value of array with eax
jg          L4                               ;if greater, jumps to L4
xchg        eax, [arrayInt + (ebx * 4) + 4]  ;if less, exchanges values
mov         [arrayInt + (ebx * 4)], eax      ;and moves new value of eax to the current value of the array

    L4:

inc         ebx                              ;increments ebx to iterate through array
loop        L3                               ;inner loop iterates through array values once

pop         ecx                              ;pops ecx that was previously pushed to reset count for outer loop
loop        L2                               ;loops back to L2 to iterate through all values of array again


    SetWrite:
mov         ebx, 0                           ;resets ebx to 0
pop         ecx                              ;pops intial count for ecx, to determine how many times 
                                             ;writeint should be called

call        Crlf                             ;prints new line
mov         edx, ExitMsg                     ;moves ExitMsg to edx
call        WriteString                      ;writes ExitMsg

    WriteArray:

mov         eax, [arrayInt + (ebx)]          ;moves values of array to eax, where WriteInt calls from
call        WriteInt                         ;writes current value of eax
call        Crlf                             ;prints new line
add         ebx, 4                           ;increments ebx by 4
loop        WriteArray                       ;loops ecx times
jmp         exit                             ;jumps to exit

    WriteArrayOne:

call        Crlf                             ;prints new line
mov         edx, ExitMsg                     ;moves ExitMsg to edx
call        WriteString                      ;writes ExitMsg

mov         eax, [arrayInt + (ebx)]          ;moves first value of array to eax, where WriteInt calls from
call        WriteInt                         ;writes value of eax
call        Crlf                             ;prints new line
jmp         exit                             ;jumps to exit

    invalidInput:                            ;jumps here if input is not an integer

mov         edx,InputError1                  ;moves InputError1 to edx
call        WriteString                      ;prints InputError1
jmp         exit                             ;exits

    tooManyInts:

mov         edx, InputError2                 ;moves InputError2 to edx
call        WriteString                      ;writes InputError2
jmp         exit                             ;exits

    badInput:

cmp         ecx, 1                           ;if ecx == 1, prints that one value
je          WriteArrayOne                    ;jumps to WriteOne which writes first int in the array

mov         edx, InputError3                 ;if zero, moves InputError3 to edx
call        WriteString                      ;writes InputError3
jmp         exit                             ;exits

    exit:                                    ;exits program
int         80h

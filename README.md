# CS330 Assembly Language

Course homework for CS330 Assembly Language at UAB. This is intended as a guide for any future students at UAB or elsewhere. DO NOT plagiarize, please!

### Details

* NASM in an i386 environment
* Uses open source libraries including:
 -  libc
 -  [along32] [1]

### Easy Compile/Link/Execute

```sh
$ sh makerun lathamd1
```

[1]:http://along32.sourceforge.net/

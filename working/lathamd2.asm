;#######################
;Daniel Latham
;BID: l3li3l
;February 23, 2015
;CS330
;Dr. Hyatt
;
;
;Homework 2
;
;Print average of all values entered along with the size of the input
;0 denotes the end of the input and is not used in the computation
;Numbers entered are signed 32-bit integers
;
;Directions:
;Must be using linux32(preferred) or linux64
;Run
;$: ./makerun32 lathamd2
;or
;$: ./makerun64 lathamd2
;or
;$: ./asm32 lathamd2
;$: ./lathamd2


%include "along32.inc"

section .data
    begin:            db "Enter number to add to list; Enter 0 to end the list and print average",0ah,0
    inputof:          db "Input error; please enter an integer",0ah,0
    inputno:          db "no-input error; please enter at least one number",0ah,0
    totalmsg:         db "Total number of integers: ",0
    averagemsg:       db "Average: ",0

section .code

global main

    main:

mov            ecx,0                         ;sets counter to 0
mov            ebx,0                         ;sets total to 0
mov            edx,begin                     ;moves begin to edx where WriteString gets its String
call           WriteString                   ;write begin
call           readInput                     ;calls readInput label

    readInput:                               ;reads the input

call           ReadInt                       ;reads integer
jo             invalidInput                  ;jumps if not an integer value
inc            ecx                           ;increments counter
add            ebx, eax                      ;adds read integer to total
cmp            eax,0                         ;tests for the 0 input
jnz            readInput                     ;loops if input not 0
jmp            calcAverage                   ;if input is zero, jumps to calcAverage

    calcAverage:                             ;calculates average of the inputs

dec            ecx                           ;decrements counter to ignore the 0 input
jecxz          noInput                       ;jumps if there was no input
mov            eax,ebx                       ;moves total to eax so it can be divided
cdq                                          ;extends short value to longer value
idiv           ecx                           ;divides total by the counter, average stored in eax

mov            edx,averagemsg                ;moves averagemsg to edx so it can be called by WriteString
call           WriteString                   ;writes averagemsg
call           WriteInt                      ;writes the value of eax, which is now the value of the average of the numbers
call           Crlf                          ;prints line

mov            edx,totalmsg                  ;moves totalmsg so it can be written by WriteString
mov            eax,ecx                       ;moves values of counter to eax so WriteInt will display that number
call           WriteString                   ;writes totalmsg
call           WriteInt                      ;writes values held by counter
call           Crlf                          ;prints new line
jmp            exit                          ;exits 

    invalidInput:                            ;jumps here if input is not an integer

mov            edx,inputof                   ;moves inputof to edx
call           WriteString                   ;prints inputof
jmp            exit                          ;exits

    noInput:                                 ;jumps here if first input is 0

mov            edx,inputno                   ;moves inputno to edx
call           WriteString                   ;prints inputno
jmp            exit                          ;exits

    exit:                                    ;exits program

mov            eax,1
int            0x80

%include "along32.inc"

section .data
val       dq   0
fpval     dq   0.0
four      dq   4
msg1      db   "Enter a floating point value. This number should be", 10, 13
          db   "echoed exactly as entered.  This validates ReadFloat", 10, 13
          db   "and WriteFloat", 10, 13, "enter value: ", 0

msg2      db   "Enter an integer value.  This number should be echoed", 10, 13
          db   "as a signed integer, then as an unsigned integer,", 10, 13,
          db   "and then as a hexadecimal value.", 10, 13
          db   "enter value: ", 0

msg3      db   "Next you should see a 5 second delay before the next", 10, 13
          db   "test is started.", 10, 13, 0

msg4      db   "Next we check the _time() interface that is used to", 10, 13
          db   "seen the random number generator.  You should see", 10, 13
          db   "the number of seconds since the epoch displayed.", 10, 13, 0

msg5      db   "Next we dump registers and flags to test DumpRegs", 10, 13, 0
section .text

global main

main:
          call Clrscr
          mov  edx, msg1
          call WriteString
          call ReadFloat
          call WriteFloat
          call Crlf
          call Crlf

          mov  edx, msg2
          call WriteString
          call ReadInt
          call WriteInt
          call Crlf
          call WriteDec
          call Crlf
          call WriteHex
          call Crlf
          call WriteBin
          call Crlf
          call Crlf

          mov  edx, msg3
          call WriteString
          mov  eax, 5000000
          call Delay
          call Crlf

          mov  edx, msg4
          call WriteString
          call Randomize
          call WriteDec
          call Crlf
          call Crlf

          finit
          fld  qword [fpval]
          fld  qword [fpval]
          fld  qword [fpval]
          fld  qword [fpval]
          fld  qword [fpval]
          fld  qword [fpval]
          fld  qword [fpval]
          fld  qword [fpval]
          mov  edx, msg5
          call WriteString
          call DumpRegs
          call Crlf
          call Crlf
          call ExitProc

;#######################
;Daniel Latham
;BID: l3li3l
;February 11, 2015
;CS330
;Dr. Hyatt
;
;
;Homework 1
;
;Print result of
;A*B - (A+B)/(A-B)
;
;Directions:
;Must be using linux32(preferred) or linux64
;Run
;$: ./makerun32 lathamd1
;or
;$: ./makerun64 lathamd1
;or
;$: ./asm32 lathamd1
;$: ./lathamd1
%include "along32.inc"

section .data 

enumA : db "Enter #A", 0ah, 0
enumB : db "Enter #B", 0ah, 0
begmsg : db "A*B - (A+B)/(A-B) = ", 0
badInput : db "Bad Input. Only use integer values", 0ah, 0
badDiv : db "Bad Divisor. Only use values s/t A != B", 0ah, 0

section .text

global main

main: 

call           readA        
push           eax           ;pushes the first input value to the stack
call           readB         
push           eax           ;pushes the second input value to the stack
                             
pop            ebx           ;moves the second input value to ebx
pop            eax           ;moves the first input value to eax
                             
call           multiply      
push           ecx           ;pushes the result of the multiplication of A and B to stacknd B to stack
                             
call           addition      
push           ecx           ;pushes the result of the addition of A and B to stacko stack
                             
call           subtract      
push           ecx           ;pushes the result of the subtraction of A and B to stackB to stack
                             
pop            ebx           ; sets (A-B) to ebx
pop            eax           ; sets (A+B) to eax
                             
call           divide        
push           ecx           ;pushes the result of the division of (A+B) by (A-B)(A-B)
                             
pop            ebx           ;moves the quotient to ebx
pop            eax           ;moves the product of A and B to eax
                             
call           subtract      ;subtraction of (A*B) and (A+B/A-B)
                             
call           displayResult ;prints the final result of the operation
                             
jmp            exit          ;exits program
                             
                             
                             
                             ;########################################
                             ;########################Util Components#
                             ;########################################
                             
                             
readA:                       ;reads input A
                             
mov            edx, enumA    ;moves enterA String to edx
call           WriteString   ;prints enterA
call           ReadInt       ;reads input to eax and tests for integer
jo             inputNotInt   ;jumps to inputNotInt if input is not an int
ret                          ;returns to main
                             
                             
                             
readB:                       ;reads input B
                             
mov            edx, enumA    ;moves enterB String to edx
call           WriteString   ;prints enterB
call           ReadInt       ;reads input to eax and tests for integer
jo             inputNotInt   ;jumps to inputNotInt if input is not an int
ret                          ;returns to main
                             
                             
                             
displayResult:               ;displays the result of the operations
                             
mov            edx, begmsg   ;moves begmsg String to edx
call           WriteString   ;prints begmsg String
mov            eax, ecx      ;moves integer result to eax
call           WriteInt      ;prints the integer result of the operations
call           Crlf          ;prints newline
                             
                             
                             
inputNotInt:                 ;displays error if input is not exactly two integers
                             
mov            edx, badInput ;moves badInput String to edx
call           WriteString   ;prints badInput message
jmp            exit          ;exits program 
                             
                             
                             
invalidDivisor:              ;displays error message if A-B = 0
                             
mov            edx, badDiv   ;moves badDiv String to edx
call           WriteString   ;prints error message
jmp            exit          
                             
                             
                             ;########################################
                             ;########################Math Components#
                             ;########################################
                             
addition:                    ;addition two values
                             
push           eax           ;pushes the value in eax to the stack
add            eax,ebx       ;addition values in eax and ebx
mov            ecx,eax       ;moves the sum to ecx
pop            eax           ;restores the value of eax
ret                          ;returns to main
                             
subtract:                    ;subtracts two values
                             
push           eax           ;pushes the value in eax to the stack
sub            eax, ebx      ;subtracts values in eax and ebx
mov            ecx, eax      ;moves the difference to ecx
pop            eax           ;restores the value of eax
ret                          ;returns to main
                             
multiply:                    ;multiplies two values eax and ebx
                             
push           eax           ;pushes the value in eax to the stack
imul           eax, ebx      ;multiplies values in eax and ebx
mov            ecx, eax      ;moves the product to ecx
pop            eax           ;restores the value of eax
ret                          ;returns to main
                             
divide:                      ;divides ecx by ebx
                             
mov            ecx, ebx      ;moves value of ecx to ebx
jecxz          invalidDivisor;jumps if ecx is zero
mov            edx, 0        ;sets edx to zero
idiv           ebx           ;divides the value of eax by ebx
mov            ecx, eax      ;moves the quotient to ecx
ret                          ;returns to main
                             
                             
exit:                        
                             
mov            eax, 1        ;move system call 1 (exit) to eax
int            0x80          ;call kernel

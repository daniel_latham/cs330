%include "Along32.inc"
 
section .data
a :  dd 0
b :  dd 0
c :  dd 0
four : dd 4
two : dd 2
res : dd 0
input: db "Please enter #: ",0ah,0
 
section .text
  global _start:
  _start:
  mov ecx,3                    ;number of times to loop
  mov ebx,0                    ;index into array
  mov edx,input
 
read:
  call WriteString
  call ReadInt
  push eax
  loop read
 
                                    ;Finish reading a,b and c
  pop dword [c]                     ;storing a,b,c
  pop dword [b]                      
  pop dword [a]
 
  fld dword [a]
 
  call WriteFloat
  fmul dword [c]                    ;a*c
  fimul dword [four]                ;4*a*c
  call WriteFloat
  fld dword [b]
  call WriteFloat
  fmul st0,st0                      ;b^2
  call WriteFloat
  fsub st0,st1                      ;b^2-4*a*c
  call WriteFloat
  fsqrt                             ;sqrt(b^2-4ac)
  call WriteFloat
  fld dword [b]
  call WriteFloat
  fchs
  fsub st0,st1                      ;(-b)-sqrt(b^2-4ac)
  call WriteFloat
  fld dword [a]
  call WriteFloat
  fimul dword [two]                       ;2a
  call WriteFloat
  fdiv st1,st0                      ;((-b)-sqrt(b^2-4ac))/(2a)
  call WriteFloat
 mov eax,1
  int 80h

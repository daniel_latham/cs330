;#######################
;Daniel Latham
;BID: l3li3l
;March 3, 2015
;CS330
;Dr. Hyatt
;
;Homework 3
;
;Print the position of the MSB and LSB and the total number of bits
;
;Directions:
;Must be using linux32(preferred) or linux64
;Run
;$: ./makerun32 lathamd3
;or
;$: ./makerun64 lathamd3
;or
;$: ./asm32 lathamd3
;$: ./lathamd3

%include "along32.inc"

section .data

    msg1 : db "Enter a 32 bit hexadecimal value: ", 0
    msg2 : db "Position of least significnat 1 bit set: ", 0
    msg3 : db "Position of most significant 1 bit set: ", 0
    msg4 : db "The total number of 1 bit sets: ", 0

section .text

global main

    main:

mov     edx, msg1                            ;move msg1 to edx where WriteString will call from
call    WriteString                          ;prints msg1
call    ReadHex                              ;reads the hex number
push    eax                                  ;pushes the value of eax which is the value of user input read in from ReadHex
                                             ;this stores the value of eax as it is about to be altered

bsf     eax, eax                             ;searches for least significant bit in eax, stores that value in eax
mov     edx, msg2                            ;moves msg2 to edx
call    WriteString                          ;prints msg2
call    WriteInt                             ;prints value of eax which will be least significant bit
call    Crlf                                 ;prints a line

pop     eax                                  ;restores value of eax
push    eax                                  ;stores value of eax once again
bsr     eax, eax                             ;searches for most significant bit set
mov     edx, msg3                            ;move msg3 to edx
call    WriteString                          ;print msg3
call    WriteInt                             ;prints current value of eax which will be most significant bit position
call    Crlf                                 ;print a line

pop     eax                                  ;restore value of eax
mov     ecx, 0                               ;sets ecx to zero, so it can track 1 bit sets in the following loop

    loop:                                    ;finds count of number of 1 bit sets

test    eax, eax                             ;does "mental" bitwise AND of eax with itself, without altering eax
jz      exit                                 ;if eax AND eax sets zero flag, jumps to exit
mov     ebx, eax                             ;moves value of eax to ebx
dec     ebx                                  ;decrements ebx
and     eax, ebx                             ;performs AND operation on eax and ebx, which is the value of eax decremented by one
                                             ;the value of the AND operation is then stored in eax
inc     ecx                                  ;increments ecx to keep track 
jmp     loop                                 ;perform loop operation again, with new value of eax

    exit:

mov     eax, ecx                             ;move count to eax
mov     edx, msg4                            ;move msg4 to edx
call    WriteString                          ;print edx/msg4
call    WriteInt                             ;print ecx/the number of 1 bit sets
call    Crlf                                 ;prints a line
int 80h

